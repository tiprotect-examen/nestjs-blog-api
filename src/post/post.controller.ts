import {
  Body, ClassSerializerInterceptor, Controller, DefaultValuePipe, Get, Param, ParseIntPipe, Post, Query, UseInterceptors
} from '@nestjs/common';
import { CreatePostDto } from './dto/create-post.dto';
import { PostService } from './post.service';

@Controller('api/posts')
export class PostController {
  constructor(private readonly postService: PostService) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @Post()
  async create(@Body() createPostDto: CreatePostDto) {
    return { data: await this.postService.create(createPostDto) };
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Get()
  async findAll(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('q') q?: string,
  ) {
    const { items, meta } = await this.postService.findAll(
      { page: page, limit: 15 },
      q,
    );

    return {
      data: items,
      meta,
    };
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Get(':id')
  async findOne(@Param('id', ParseIntPipe) id: number) {
    return { data: await this.postService.findOne(id) };
  }
}
