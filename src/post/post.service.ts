import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { Post } from './entities/post.entity';
import {
  paginate,
  Pagination,
  IPaginationOptions,
  IPaginationMeta,
} from 'nestjs-typeorm-paginate';
import { title } from 'process';

export class Meta {
  constructor(
    private readonly from: number,
    private readonly to: number,
    private readonly current_page: number,
    private readonly per_page: number,
    private readonly last_page: number,
    private readonly total: number,
  ) {}
}

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(Post)
    private postRepository: Repository<Post>,
  ) {}

  create(createPostDto: CreatePostDto) {

    const post: Post = new Post();
    post.title = createPostDto.title
    post.author = createPostDto.author
    post.content = createPostDto.content
    
    return this.postRepository.save(post);
  }

  async findAll(
    options: IPaginationOptions,
    q?: string,
  ): Promise<Pagination<Post, Meta>> {
    const queryBuilder = this.postRepository.createQueryBuilder('p');

    if (q != undefined) {
      queryBuilder.where(
        'p.title LIKE :title OR p.author LIKE :author OR p.content LIKE :content',
        { title: `%${q}%`, author: `%${q}%`, content: `%${q}%` },
      );
    }

    console.log(queryBuilder.orderBy('p.created_at', 'DESC').getSql());
    return await paginate<Post, Meta>(queryBuilder, {
      ...options,
      metaTransformer: (meta: IPaginationMeta) =>
        new Meta(
          meta.itemsPerPage * (meta.currentPage - 1) + 1,
          meta.itemCount + meta.itemsPerPage * (meta.currentPage - 1),
          meta.currentPage,
          meta.itemsPerPage,
          meta.totalPages,
          meta.totalItems,
        ),
    });
  }

  findOne(id: number) {
    return this.postRepository.findOneByOrFail({ id });
  }
}
